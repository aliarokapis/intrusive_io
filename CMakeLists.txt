cmake_minimum_required(VERSION 3.10)
project(intrusive_io)

option(INTRUSIVE_IO_USE_CONAN_LIBRARIES "Use conan for packages. Set to ON to use conan" OFF)
option(INTRUSIVE_IO_INSTALL_EXTERNAL_SPANLITE "Install external span-lite dependency in include/intrusive_io/external" ON)
option(INTRUSIVE_IO_BUILD_SAMPLES "Build samples" OFF)
option(INTRUSIVE_IO_BUILD_TESTS "Build tests" OFF)

if(NOT ${INTRUSIVE_IO_USE_CONAN_LIBRARIES})
    find_package(Boost REQUIRED)

    set(INTRUSIVE_IO_BOOST_INCLUDE_DIR ${Boost_INCLUDE_DIRS})
else()
    include(conanbuildinfo.cmake)
    conan_basic_setup(TARGETS)

    get_property(INTRUSIVE_IO_BOOST_INCLUDE_DIR TARGET CONAN_PKG::boost
      PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
endif()

if(WIN32)
  set(CLANG_TIDY_HINT "C:/Program Files/LLVM/bin")

  add_compile_definitions(_SILENCE_CXX17_ALLOCATOR_VOID_DEPRECATION_WARNING=1)
else()
  set(CLANG_TIDY_HINT "")
endif()

find_program(
  CLANG_TIDY_EXE
  NAMES "clang-tidy"
  HINTS ${CLANG_TIDY_HINT}
  DOC "Path to clang-tidy executable"
  )

set(CXX_STANDARD 17)
set(CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(intrusive_io_sources
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/callable_wrapper.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/chrono.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/execution_agent.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/execution_resource.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/executor_invoking_service.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/basic_service.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/platform.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/timeout_timer.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/timer_service.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/data_buffer_service.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/producer_consumer_service.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/pin_service.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/generic_data_service.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/commit_span.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/ring_span.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/listener_list.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/span.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/error.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/dispatch_policy.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/multilistener_notifier.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/experimental/buffered_input_stream.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/experimental/output_stream.hpp>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include/intrusive_io/experimental/future.hpp>
  )

add_library(intrusive_io INTERFACE)

target_sources(intrusive_io INTERFACE 
    ${intrusive_io_sources})

message("Boost include dir = ${INTRUSIVE_IO_BOOST_INCLUDE_DIR}")

target_include_directories(intrusive_io BEFORE INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/external/span-lite/include>
  $<BUILD_INTERFACE:${INTRUSIVE_IO_BOOST_INCLUDE_DIR}>

  $<INSTALL_INTERFACE:include>
)

if(${INTRUSIVE_IO_INSTALL_EXTERNAL_SPANLITE})
  target_include_directories(intrusive_io BEFORE INTERFACE
    $<INSTALL_INTERFACE:include/intrusive_io/external>
  )
endif()

include(GNUInstallDirs)

set(INTRUSIVE_IO_CMAKE_DIR ${CMAKE_INSTALL_LIBDIR}/cmake/intrusive_io CACHE STRING
"Installation directory for cmake files, relative to ${CMAKE_INSTALL_PREFIX}.")

install(TARGETS intrusive_io EXPORT intrusive_io)
install(
  DIRECTORY include/intrusive_io
    DESTINATION include
  )

if(${INTRUSIVE_IO_INSTALL_EXTERNAL_SPANLITE})
  install(DIRECTORY
    external/span-lite/include/nonstd
    DESTINATION include/intrusive_io/external
  )
endif()

install(FILES intrusive_io-config.cmake DESTINATION ${INTRUSIVE_IO_CMAKE_DIR})
install(EXPORT intrusive_io DESTINATION ${INTRUSIVE_IO_CMAKE_DIR})

option(INTRUSIVE_IO_RUN_CLANG_TIDY "Run clang-tidy checks in build process" OFF)

if(NOT CLANG_TIDY_EXE)
  message(STATUS "clang-tidy not found.")
elseif(NOT INTRUSIVE_IO_RUN_CLANG_TIDY)
  message(STATUS "skipping clang-tidy")
else()
  message(STATUS "clang-tidy found: ${CLANG_TIDY_EXE}")
  set(TIDY_CHECKS "-checks=*")
  set(TIDY_CHECKS "${TIDY_CHECKS},-hicpp-uppercase-literal-suffix,-clang-analyzer-alpha.*,-absail*,-google*,-misc-non-private-member-*")
  SET(TIDY_CHECKS "${TIDY_CHECKS},-fuchsia-overloaded-operator,-llvm-header-guard,-fuchsia-default-arguments")
  SET(TIDY_CHECKS "${TIDY_CHECKS},-modernize-concat-nested-namespaces")
  set(DO_CLANG_TIDY "${CLANG_TIDY_EXE}" "${TIDY_CHECKS}")
  
  get_property(ALL_INCLUDE_DIRS TARGET intrusive_io PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
  foreach(DIR IN LISTS ALL_INCLUDE_DIRS)
    string(FIND "${DIR}" "INSTALL_INTERFACE" INST_FOUND)
    if(${INST_FOUND} EQUAL -1)
      list(APPEND CLANG_TIDY_INCLUDES -I${DIR})
    endif()
  endforeach()
  add_custom_target(intrusive_io_clang_check "${CLANG_TIDY_EXE}" ${CLANG_TIDY_OPTIONS} ${intrusive_io_sources}
    --
    -std=c++17
    ${CLANG_TIDY_INCLUDES}
  )
endif()

if(INTRUSIVE_IO_BUILD_SAMPLES)
  add_subdirectory(samples)
endif()

if(INTRUSIVE_IO_BUILD_TESTS)
  enable_testing()
  add_subdirectory(tests)
endif()
