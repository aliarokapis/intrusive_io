#pragma once

#include <algorithm>
#include <array>

#include "intrusive_io/producer_consumer_service.hpp"
#include "intrusive_io/span.hpp"

namespace intrusive_io
{
namespace experimental
{
template <class StreamT>
class buffered_output_peripheral;

namespace detail
{
template <class StreamT>
class output_stream_base
{
public:
    using value_type = StreamT;
    using buffer_type = span<StreamT>;
    output_stream_base() noexcept = default;
    virtual ~output_stream_base() noexcept = default;
    output_stream_base(const output_stream_base &) noexcept = default;
    output_stream_base(output_stream_base &&) noexcept = default;

    output_stream_base &operator=(const output_stream_base &) noexcept = default;
    output_stream_base &operator=(output_stream_base &&) noexcept = default;

protected:
    virtual void notify_output_consumed(data_consumer<buffer_type> &consumer) noexcept = 0;

    virtual data_consumer<buffer_type> make_output_consumer(
        typename data_consumer<buffer_type>::completion_handler completion) noexcept = 0;

    virtual void async_consume_output(data_consumer<buffer_type> &consumer) noexcept = 0;

    friend class buffered_output_peripheral<value_type>;
};
} // namespace detail

template <class StreamT, std::size_t NumOutputSlots>
class output_stream : public detail::output_stream_base<StreamT>
{
public:
    using value_type = StreamT;
    using buffer_type = span<StreamT>;

    explicit output_stream(execution_resource &executor) noexcept : executor_(&executor) {

        for (auto &slot : write_slots_) {
            slot.producer_ = write_service_.make_producer(
                *executor_, wrap_member_fn<&output_stream::on_producer_done>(*this));
        }
    }

    bool async_write(buffer_type buffer) noexcept {
        auto *write_slot = get_available_slot();
        if (write_slot) {
            write_slot->in_use_ = true;
            write_service_.async_produce_data(write_slot->producer_, buffer);
            return true;
        }
        return false;
    }

    bool async_write(span<buffer_type> buffers) noexcept {
        for (auto buf : buffers) {
            if (!async_write(buf)) {
                return false;
            }
        }
        return true;
    }

    void async_wait_all_written(callable_wrapper<void()> completion_handler) noexcept {
        on_all_written_ = completion_handler;
    }

protected:
    void notify_output_consumed(data_consumer<buffer_type> &consumer) noexcept override {
        write_service_.notify_consumed_done(status_code(status_value::success), consumer);
    }

    data_consumer<buffer_type> make_output_consumer(
        typename data_consumer<buffer_type>::completion_handler completion) noexcept override {
        return write_service_.make_consumer(*executor_, completion);
    }

    void async_consume_output(data_consumer<buffer_type> &consumer) noexcept override {
        write_service_.async_consume_data(consumer);
    }

private:
    struct write_slot_type
    {
        bool in_use_ = false;
        data_producer<buffer_type> producer_;
    };

    void on_producer_done(status_code code, data_producer<buffer_type> *producer) {
        if (code.success()) {
            auto producer_item =
                std::find_if(write_slots_.begin(), write_slots_.end(),
                             [producer](auto &slot) { return &slot.producer_ == producer; });

            if (producer_item != write_slots_.end()) {
                producer_item->in_use_ = false;
            }

            if (num_outputs_in_use() == 0) {
                execution_agent_ = executor_->make_agent(on_all_written_);
                on_all_written_ = callable_wrapper<void()>{};
                dispatch(execution_agent_, *executor_);
            }
        }
    }

    std::size_t num_outputs_in_use() {
        return std::count_if(write_slots_.begin(), write_slots_.end(),
                             [](auto &slot) { return slot.in_use_; });
    }

    write_slot_type *get_available_slot() {
        auto first_available = std::find_if(write_slots_.begin(), write_slots_.end(),
                                            [](auto &slot) { return !slot.in_use_; });
        if (first_available != write_slots_.end()) {
            return &(*first_available);
        }

        return nullptr;
    }

    execution_resource *executor_;
    execution_resource::execution_agent_type execution_agent_;

    callable_wrapper<void()> on_all_written_;
    producer_consumer_service<buffer_type> write_service_;
    std::array<write_slot_type, NumOutputSlots> write_slots_;
};

template <class StreamT>
class buffered_output_peripheral
{
public:
    using buffer_type = typename detail::output_stream_base<StreamT>::buffer_type;
    using value_type = typename detail::output_stream_base<StreamT>::value_type;
    using completion_handler = typename data_consumer<buffer_type>::completion_handler;

    constexpr buffered_output_peripheral() noexcept = default;

    explicit buffered_output_peripheral(detail::output_stream_base<StreamT> &stream) noexcept
        : stream_(&stream) {
        consumer_ = stream_->make_output_consumer({});
    }

    void notify_data_consumed() {
        if (stream_) {
            stream_->notify_output_consumed(consumer_);
        }
    }

    void async_wait_data() {
        if (stream_) {
            stream_->async_consume_output(consumer_);
        }
    }

    void async_wait_data(completion_handler completion) {
        if (stream_) {
            consumer_.set_completion_handler(completion);
            stream_->async_consume_output(consumer_);
        }
    }

private:
    detail::output_stream_base<StreamT> *stream_ = nullptr;
    data_consumer<buffer_type> consumer_;
};
} // namespace experimental
} // namespace intrusive_io