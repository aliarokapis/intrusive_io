#pragma once

#include "intrusive_io/data_buffer_service.hpp"

namespace intrusive_io
{
namespace experimental
{
template <class StreamT>
class buffered_input_peripheral;

namespace detail
{
template <class StreamT>
class buffered_input_stream_base
{
public:
    buffered_input_stream_base() noexcept = default;
    buffered_input_stream_base(const buffered_input_stream_base &rhs) noexcept = default;
    buffered_input_stream_base(buffered_input_stream_base &&rhs) noexcept = default;
    virtual ~buffered_input_stream_base() noexcept = default;

    buffered_input_stream_base &operator=(const buffered_input_stream_base &rhs) noexcept = default;
    buffered_input_stream_base &operator=(buffered_input_stream_base &&rhs) noexcept = default;

protected:
    using value_type = StreamT;
    using buffer_type = span<StreamT>;

    virtual void notify_input(buffer_type data) noexcept = 0;

    friend class buffered_input_peripheral<StreamT>;
};
} // namespace detail

template <class StreamT, std::size_t InputBufferSize>
class buffered_input_stream : public detail::buffered_input_stream_base<StreamT>
{
public:
    using value_type = typename detail::buffered_input_stream_base<StreamT>::value_type;
    using buffer_type = typename detail::buffered_input_stream_base<StreamT>::buffer_type;

    explicit buffered_input_stream(execution_resource &executor) noexcept
        : input_buffer_service_(executor) {
        input_listener_ = input_buffer_service_.make_listener(executor, {});
    }
    void async_read_some(
        execution_resource &executor, buffer_type buffer,
        data_buffer_service_listener<char>::completion_handler completion_handler) noexcept {
        input_listener_ = input_buffer_service_.make_listener(executor, completion_handler);
        input_buffer_service_.async_read_some(input_listener_, buffer);
    }

    void async_read_some(
        buffer_type buffer,
        data_buffer_service_listener<char>::completion_handler completion_handler) noexcept {
        input_listener_.set_completion_handler(completion_handler);
        input_buffer_service_.async_read_some(input_listener_, buffer);
    }

    void async_read_some(buffer_type buffer) noexcept {
        input_buffer_service_.async_read_some(input_listener_, buffer);
    }

protected:
    void notify_input(buffer_type data) noexcept override {
        input_buffer_service_.notify(data);
    }

private:
    data_buffer_service<StreamT, InputBufferSize> input_buffer_service_;
    data_buffer_service_listener<StreamT> input_listener_;
};

template <class StreamT>
class buffered_input_peripheral
{
public:
    using buffer_type = typename detail::buffered_input_stream_base<StreamT>::buffer_type;
    using value_type = typename detail::buffered_input_stream_base<StreamT>::value_type;

    buffered_input_peripheral() noexcept = default;
    explicit buffered_input_peripheral(detail::buffered_input_stream_base<StreamT> &stream) noexcept
        : stream_(&stream) {
    }

    void notify(buffer_type data) {
        if (stream_) {
            stream_->notify_input(data);
        }
    }
    void notify(value_type data) {
        notify({&data, 1});
    }

private:
    detail::buffered_input_stream_base<StreamT> *stream_ = nullptr;
};
} // namespace experimental
} // namespace intrusive_io