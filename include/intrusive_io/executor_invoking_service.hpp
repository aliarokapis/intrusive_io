/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "basic_service.hpp"
#include "execution_resource.hpp"
#include "platform.hpp"

#include <algorithm>
#include <type_traits>

namespace intrusive_io
{

namespace detail
{
struct executor_invoking_listener_tag
{
};
} // namespace detail

template <class ListenerT>
using is_executor_invoking_listener =
    std::is_convertible<ListenerT *, detail::executor_invoking_listener_tag *>;

template <class ListenerT>
constexpr bool is_executor_invoking_listener_v = is_executor_invoking_listener<ListenerT>::value;

template <class Impl, class NotifyArgs,
          class ListenerArgs = typename NotifyArgs::listener_argument_type>
struct executor_invoking_listener;

template <class Impl, class... NotifyArgs, class ListenerArgs>
struct executor_invoking_listener<Impl, notify_arguments<NotifyArgs...>, ListenerArgs>
    : public basic_service_listener<notify_arguments<NotifyArgs...>, ListenerArgs>,
      public detail::executor_invoking_listener_tag
{
private:
    using base_ = basic_service_listener<notify_arguments<NotifyArgs...>, ListenerArgs>;

public:
    using completion_handler = typename base_::completion_handler;

    executor_invoking_listener() noexcept {
        static_assert(std::is_base_of_v<executor_invoking_listener, Impl>,
                      "service_listener must be a base of Impl");
    }

    executor_invoking_listener(execution_resource &executor,
                               typename base_::completion_handler listener) noexcept
        : base_(executor, listener), executor_(std::addressof(executor)) {

        static_assert(std::is_base_of_v<executor_invoking_listener, Impl>,
                      "service_listener must be a base of Impl");
    }

public:
    void notify_hook(const NotifyArgs &... args) noexcept {
        static_assert(!std::is_same_v<decltype(&Impl::notify_hook),
                                      decltype(&executor_invoking_listener::notify_hook)>,
                      "Impl template argument type must implement a notify_hook function");

        static_assert(std::is_invocable_v<decltype(&Impl::notify_hook), Impl *, NotifyArgs...>,
                      "Impl::notify_hook does not take the correct arguments");

        auto *this_impl = static_cast<Impl *>(this);
        this_impl->notify_hook(args...);
    }

    void invoke_hook() noexcept {
        static_assert(!std::is_same_v<decltype(&Impl::invoke_hook),
                                      decltype(&executor_invoking_listener::invoke_hook)>,
                      "Impl template argument must implement an invoke_hook() function");

        static_assert(std::is_invocable_v<decltype(&Impl::invoke_hook), Impl *>,
                      "Impl::invoke_hook does not take the correct arguments");

        if (executor_) {
            auto *this_impl = static_cast<Impl *>(this);
            listener_agent_ = executor_->make_agent(wrap_member_fn<&Impl::invoke_hook>(*this_impl));
            intrusive_io::dispatch(listener_agent_, *executor_);
        }
    }

    void cancel_hook() noexcept {
        if (executor_) {
            auto *this_impl = static_cast<Impl *>(this);
            listener_agent_ = executor_->make_agent(wrap_member_fn<&Impl::cancel_hook>(*this_impl));
            intrusive_io::dispatch(listener_agent_, *executor_);
        }
    }

    execution_resource *executor_ = nullptr;
    execution_resource::execution_agent_type listener_agent_;
};

namespace detail
{
template <class... ExecutorListenerArgs>
auto to_executor_invoking_listener(executor_invoking_listener<ExecutorListenerArgs...> &&)
    -> executor_invoking_listener<ExecutorListenerArgs...>;
} // namespace detail

template <class ListenerT>
using as_executor_invoking_listener =
    decltype(detail::to_executor_invoking_listener(std::declval<ListenerT>()));

template <class ListenerT>
using executor_invoking_service = basic_service<executor_invoking_listener<
    ListenerT, typename listener_traits<ListenerT>::notify_arguments_type,
    typename listener_traits<ListenerT>::listener_arguments_type>>;

template <class A, class B, class C>
void bind_executor(executor_invoking_listener<A, B, C> &listener, execution_resource &executor) {
    listener.executor_ = &executor;
}

template <class A, class B, class C, class D, class E, class F>
void bind_to_listener_executor(executor_invoking_listener<A, B, C> &target,
                               executor_invoking_listener<D, E, F> &from) {
    target.executor_ = from.executor_;
}
} // namespace intrusive_io
