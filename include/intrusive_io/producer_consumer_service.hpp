/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "intrusive_io/dispatch_policy.hpp"
#include "intrusive_io/error.hpp"
#include "intrusive_io/executor_invoking_service.hpp"

namespace intrusive_io
{
template <class DataT>
struct data_producer
    : public executor_invoking_listener<data_producer<DataT>, notify_arguments<status_code>,
                                        listener_arguments<status_code, data_producer<DataT> *>>
{
    using base_ =
        executor_invoking_listener<data_producer, notify_arguments<status_code>,
                                   listener_arguments<status_code, data_producer<DataT> *>>;
    using completion_handler = typename base_::completion_handler;
    using data_type = DataT;

    data_producer() noexcept = default;

    data_producer(execution_resource &executor, completion_handler completion_handler) noexcept
        : base_(executor, completion_handler) {
    }
    void notify_hook(status_code code) noexcept {
        status_ = code.value();
    }

    void invoke_hook() noexcept {
        in_use_ = false;
        this->invoke_completion_handler(status_code(status_), this);
    }

    void cancel_hook() noexcept {
        in_use_ = false;
        this->invoke_completion_handler(status_value::operation_canceled, this);
    }

    [[nodiscard]] bool is_in_use() const noexcept {
        return in_use_;
    }

    DataT data_; // NOLINT

private:
    bool in_use_ = false;
    status_value status_ = status_value::unknown;

    template <class>
    friend class producer_consumer_service;

    void dispatch_listener(status_code code) noexcept {
        status_ = code.value();
        this->listener_agent_ =
            this->executor_->make_agent(wrap_member_fn<&data_producer::invoke_hook>(*this));
        this->executor_->dispatch(this->listener_agent_);
    }
};

template <class DataT>
struct data_consumer : public executor_invoking_listener<data_consumer<DataT>, notify_arguments<>,
                                                         listener_arguments<status_code, DataT>>
{
    using base_ = executor_invoking_listener<data_consumer, notify_arguments<>,
                                             listener_arguments<status_code, DataT>>;
    using completion_handler = typename base_::completion_handler;
    using data_type = DataT;

    data_consumer() noexcept = default;

    data_consumer(execution_resource &executor, completion_handler completion_handler) noexcept
        : base_(executor, completion_handler) {
    }
    void notify_hook() noexcept {
    }

    void invoke_hook() noexcept {
        this->invoke_completion_handler(status_, data);
    }

    void cancel_hook() noexcept {
        this->invoke_completion_handler(status_value::operation_canceled, data);
    }
    // The consumer is allowed to modify data as it sees fit
    data_type data; // NOLINT

private:
    status_value status_ = status_value::unknown;
    data_producer<data_type> *producer_ = nullptr;

    template <class>
    friend class producer_consumer_service;

    void dispatch_listener(status_code code, dispatch_policy policy) noexcept {
        status_ = code.value();
        if (policy == dispatch_policy::direct) {
            invoke_hook();
        }
        else if (this->executor_) {
            this->listener_agent_ =
                this->executor_->make_agent(wrap_member_fn<&data_consumer::invoke_hook>(*this));
            dispatch(this->listener_agent_, *this->executor_);
        }
    }
};

template <class DataT>
class producer_consumer_service
{
public:
    using data_type = DataT;

    using producer_type = data_producer<DataT>;
    using consumer_type = data_consumer<DataT>;

    using producer_completion_handler = typename producer_type::completion_handler;
    using consumer_completion_handler = typename consumer_type::completion_handler;

    void async_consume_data(consumer_type &consumer, dispatch_policy policy = dispatch_policy::deferred) {
        lock_.lock();
        auto *producer = static_cast<producer_type *>(producers_.take_one_listener());
        if (producer) {
            lock_.unlock(); // unlock before produce_consume
            produce_consume(*producer, consumer, policy);
        }
        else {
            consumers_.async_listen_one(consumer);
            lock_.unlock(); // unlock after listening
        }
    }

    void async_produce_data(producer_type &producer, DataT data) {
        lock_.lock();
        producer.data_ = data;
        producer.in_use_ = true;
        auto *consumer = static_cast<consumer_type *>(consumers_.take_one_listener());
        if (consumer) {
            lock_.unlock(); // unlock before produce_consume
            produce_consume(producer, *consumer, dispatch_policy::deferred);
        }
        else {
            producers_.async_listen_one(producer);
            lock_.unlock(); // unlock after listening
        }
    }

    void notify_consumed_done(status_code code, consumer_type &consumer) {
        if (consumer.producer_) {
            consumer.producer_->dispatch_listener(code);
            consumer.producer_ = nullptr;
        }
    }

    void cancel(producer_type &producer) {
        producers_.cancel(producer);
    }

    void cancel(consumer_type &consumer) {
        consumers_.cancel(consumer);
    }

    producer_type make_producer(execution_resource &listener_executor,
                                producer_completion_handler completion_handler) noexcept {
        return producer_type(listener_executor, completion_handler);
    }

    consumer_type make_consumer(execution_resource &listener_executor,
                                consumer_completion_handler completion_handler) noexcept {
        return consumer_type(listener_executor, completion_handler);
    }

private:
    void produce_consume(producer_type &producer, consumer_type &consumer, dispatch_policy policy) {
        consumer.producer_ = &producer;
        consumer.data = producer.data_;
        consumer.dispatch_listener(status_value::success, policy);
    }

    platform::irq_safe_lock lock_;
    executor_invoking_service<consumer_type> consumers_;
    executor_invoking_service<producer_type> producers_;
};

} // namespace intrusive_io
