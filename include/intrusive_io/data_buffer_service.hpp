/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "intrusive_io/error.hpp"
#include "intrusive_io/execution_resource.hpp"
#include "intrusive_io/executor_invoking_service.hpp"
#include "intrusive_io/platform.hpp"
#include "intrusive_io/ring_span.hpp"
#include "intrusive_io/span.hpp"

#include <algorithm>
#include <array>
#include <cmath>

namespace intrusive_io
{
namespace detail
{
struct internal_data_buffer_service_listener
    : public executor_invoking_listener<internal_data_buffer_service_listener, notify_arguments<>>
{
public:
    using base_type =
        executor_invoking_listener<internal_data_buffer_service_listener, notify_arguments<>>;
    using completion_handler = typename base_type::completion_handler;

    internal_data_buffer_service_listener(execution_resource &executor,
                                          completion_handler listener) noexcept
        : base_type(executor, listener) {
    }
    void notify_hook() noexcept {
    }
    void invoke_hook() noexcept {
        this->invoke_completion_handler();
    }
    void cancel_hook() noexcept {
        this->invoke_completion_handler();
    }
};
} // namespace detail

template <class ValueType>
struct data_buffer_service_listener
    : basic_service_listener<notify_arguments<span<ValueType>>,
                             listener_arguments<status_code, span<ValueType>>>
{
    using base_ = basic_service_listener<notify_arguments<span<ValueType>>,
                                         listener_arguments<status_code, span<ValueType>>>;
    using completion_handler = typename base_::completion_handler;

    data_buffer_service_listener() noexcept = default;

    data_buffer_service_listener(execution_resource &executor, completion_handler listener) noexcept
        : base_(executor, listener), executor(&executor) {
    }

    void notify_hook(span<ValueType> buffer_taken) {
        buffer = buffer_taken;
    }

    void invoke_hook() noexcept {
        this->invoke_completion_handler(status_code(status_value::success), buffer);
    }

    void cancel_hook() noexcept {
        dispatch_cancel();
    }

    void invoke_canceled() {
        this->invoke_completion_handler(status_code(status_value::operation_canceled), buffer);
    }

    void dispatch_listener() noexcept {
        if (executor) {
            execution_agent_ = executor->make_agent(
                wrap_member_fn<&data_buffer_service_listener::invoke_hook>(*this));
            dispatch(execution_agent_, *executor);
        }
    }

    void dispatch_cancel() noexcept {
        if (executor) {
            execution_agent_ = executor->make_agent(
                wrap_member_fn<&data_buffer_service_listener::invoke_canceled>(*this));

            dispatch(execution_agent_, *executor);
        }
    }

    span<ValueType> buffer;                 // NOLINT
    execution_resource *executor = nullptr; // NOLINT

private:
    execution_resource::execution_agent_type execution_agent_;

    template <class, std::size_t>
    friend class data_buffer_service;
};

template <class ValueType, std::size_t BufferSize>
class data_buffer_service
    : private executor_invoking_service<detail::internal_data_buffer_service_listener>
{
    using data_buffer_base =
        executor_invoking_service<detail::internal_data_buffer_service_listener>;
    using priv_completion_handler =
        typename detail::internal_data_buffer_service_listener::completion_handler;
    using listener_storage_type =
        basic_service<data_buffer_service_listener<ValueType>, basic_service_locker::empty_locker>;

public:
    using value_type = ValueType;
    using reference = value_type &;
    using const_reference = const value_type &;
    using span_type = span<value_type>;
    using const_span_type = span<const value_type>;
    static constexpr std::size_t capacity_value = BufferSize;

    void notify(const_reference data) noexcept {
        add_data({&data, 1});
        data_buffer_base::notify();
    }

    void notify(const_span_type data) noexcept {
        add_data(data);
        data_buffer_base::notify();
    }

    explicit data_buffer_service(execution_resource &service_executor) noexcept
        : ring_buffer_(buffered_data_.begin(), buffered_data_.end()),
          internal_listener_(service_executor,
                             wrap_member_fn<&data_buffer_service::internal_listener_done>(*this)) {
        static_assert(std::is_default_constructible_v<value_type>,
                      "ValueType must be default constructible");
        static_assert(std::is_copy_assignable_v<value_type>, "ValueType must be copy assignable");
        static_assert(!std::is_const_v<value_type>, "ValueType must not be const qualified");

        async_listen_one(internal_listener_);
    }

    void async_read_some(data_buffer_service_listener<value_type> &listener,
                         span_type buffer) noexcept {
        listener.buffer = buffer;
        if (!listener.executor) {
            return;
        }

        auto data_read = take_data(listener.buffer);
        if (data_read.size()) {
            listener.buffer = data_read;
            listener.execution_agent_ = listener.executor->make_agent(
                wrap_member_fn<&data_buffer_service_listener<ValueType>::invoke_hook>(listener));
            dispatch(listener.execution_agent_, *listener.executor);
        }
        else {
            platform::irq_lock_guard guard{lock_};
            listeners_.async_listen_one(listener);
        }
    }

    void cancel(data_buffer_service_listener<value_type> &listener) noexcept {
        listeners_.cancel(listener);
    }

    data_buffer_service_listener<ValueType>
    make_listener(execution_resource &executor,
                  callable_wrapper<void(status_code, span_type)> listener) noexcept {
        data_buffer_service_listener<ValueType> retval(executor, listener);
        return retval;
    }

private:
    std::array<value_type, capacity_value> buffered_data_;
    ring_span<value_type> ring_buffer_;
    detail::internal_data_buffer_service_listener internal_listener_;
    listener_storage_type listeners_;

    platform::irq_safe_lock lock_;

    void internal_listener_done() noexcept {
        async_listen_one(internal_listener_);

        data_buffer_service_listener<value_type> *local_listener = nullptr;
        {
            platform::irq_lock_guard guard{lock_};
            if (!ring_buffer_.empty()) {
                local_listener = listeners_.take_one_listener();
                if (local_listener) {
                    local_listener->buffer = take_data(local_listener->buffer, guard);
                }
            }
        }
        if (local_listener) {
            if (local_listener->executor == internal_listener_.executor_) {
                // If the listeners executor is the same as my executor we can just
                // fast call the listener, otherwise we need to dispatch it.
                local_listener->invoke_hook();
            }
            else {
                local_listener->dispatch_listener();
            }
        }
    }

    void add_data(const_span_type data) noexcept {
        platform::irq_lock_guard guard{lock_};
        std::copy(data.begin(), data.end(), std::back_inserter(ring_buffer_));
    }

    span_type take_data(span_type buffer) noexcept {
        platform::irq_lock_guard guard{lock_};
        return take_data(buffer, guard);
    }

    span_type take_data(span_type buffer, platform::irq_lock_guard &guard) noexcept {
        (void)guard;
        if (!ring_buffer_.empty()) {
            auto buffer_size = static_cast<std::size_t>(buffer.size());
            auto ring_size = ring_buffer_.size();
            auto num_to_copy = std::min(buffer_size, ring_size);

            std::copy_n(ring_buffer_.begin(), num_to_copy, buffer.begin());

            ring_buffer_ = decltype(ring_buffer_){buffered_data_.begin(), buffered_data_.end()};

            return buffer.subspan(0, num_to_copy);
        }

        return {};
    }
};
} // namespace intrusive_io