/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#ifdef _MSVC_LANG
#define INTRUSIVE_IO_CPP_VERSION _MSVC_LANG
#else
#define INTRUSIVE_IO_CPP_VERSION __cplusplus // NOLINT
#endif

#if INTRUSIVE_IO_CPP_VERSION < 201703L
#error "Intrusive IO must be built with C++ 2017 or later"
#endif

#undef INTRUSIVE_IO_CPP_VERSION

#include "boost/intrusive/slist.hpp"
#include "execution_agent.hpp"
#include "platform.hpp"

namespace intrusive_io
{
/**
 * @brief Every execution_resource represents a thread.
 *
 * Execution resources are what drives the framework. `execution_agent` instances
 * can be dispatched to a resource, and the resource will queue
 * that agent for execution.
 */
class execution_resource
{
    struct agent_base
        : public boost::intrusive::slist_base_hook<
              boost::intrusive::link_mode<boost::intrusive::link_mode_type::normal_link>>
    {
        // Cannot default the constructor since boost slist isn't noexcept constructible
        // so the default noexcept constructor is deleted.
        agent_base() noexcept { // NOLINT
        }
    };

public:
    /**
     * @brief This is the type of execution agent that this execution resource accepts.
     *
     * Other agents can be used by
     * inhereting from this class, however at the moment
     * `execution_agent` does not support any hooks.
     */
    using execution_agent_type = execution_agent<agent_base>;

    /**
     * @brief This function runs the main execution loop.
     *
     * A semaphore is used to synchronize when work is available,
     * and work will be performed on a FIFO basis.
     */
    [[noreturn]] void run() noexcept {
        while (true) {
            semaphore_.wait();
            execute_one();
        }
    }

    /**
     * @brief Execute until no more work is queued, then return.
     * @return The number of agents that were executed.
     *
     * Run as much work as possible, until the function would block,
     * then return.
     *
     */
    int try_run() noexcept {
        int i = 0;
        while (semaphore_.try_wait()) {
            execute_one();
            i++;
        }
        return i;
    }

    /**
     * @brief Execute until `complete_predicate` returns true
     * 
     * @tparam CompletionPredicateT A function to check for completion.
     *                              must have signature `bool()`
     * 
     * @param complete_predicate The predicate to decide when to stop running.
     * 
     */
    template<class CompletionPredicateT>
    void run_until(const CompletionPredicateT &complete_predicate) {
        while(!complete_predicate()) {
            if(semaphore_.wait(complete_predicate)) {
                execute_one();
            }
        }
    }

    /**
     * @brief Queue an agent for execution
     * @param agent The agent to queue
     *
     * This will add the `execution_agent` to the list of pending
     * executions to perform.
     *
     * `dispatch` will never execute the agent directly.
     */
    void dispatch(execution_agent_type &agent) noexcept {
        {
            platform::irq_lock_guard guard(lock_);
            dispatched_work_.push_back(agent);
        }
        semaphore_.signal();
    }

    void signal_all_waiting_predicate() noexcept {
        {
            platform::irq_lock_guard guard{lock_};
        }
        semaphore_.signal_all_waiting_on_predicate();
    }

    /**
     * @brief Helper function to create an `execution_agent` with a given callable.
     * @param callable The callable that should be executed when the agent runs.
     *
     * The callable must have the signature `void()`
     */
    template <class CallableT>
    [[nodiscard]] execution_agent_type make_agent(const CallableT &callable) noexcept {
        return execution_agent_type{callable};
    }

private:
    void execute_one() noexcept {
        // Take one job
        execution_agent_type *job = nullptr;
        {
            platform::irq_lock_guard guard(lock_);
            if (!dispatched_work_.empty()) {
                job = std::addressof(dispatched_work_.front());
                dispatched_work_.pop_front();
            }
        }
        if (job != nullptr) {
            auto handler = job->get_handler();
            std::atomic_signal_fence(std::memory_order_acq_rel);
            // Will always only operate on a copy of the handler
            // in case "handler" is destroyed/recreated when
            // we call handler.
            handler();
        }
    }
    platform::irq_safe_lock lock_;
    platform::semaphore semaphore_;

    boost::intrusive::slist<execution_agent_type, boost::intrusive::cache_last<true>>
        dispatched_work_;
};

inline bool dispatch(execution_resource::execution_agent_type &agent, execution_resource *executor)
{
    // NOLINTNEXTLINE
    if(executor) {
        executor->dispatch(agent);
    }
    return false;
}

inline bool dispatch(execution_resource::execution_agent_type &agent, execution_resource &executor)
{
    executor.dispatch(agent);
    return true;
}

} // namespace intrusive_io
