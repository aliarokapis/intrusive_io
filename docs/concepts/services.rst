Services
========

.. toctree::
  :hidden:

Services provides the main abstractions between a peripheral
and the application code. They can be notified by a peripheral,
for instance via an interrupt, and they will signal one or multiple
listeners of the notification.

Notifications can also carry additional data. For instance
the ``timer_service`` template requires that notifications
tells the timer service what the timer interval is.

Service notifications
---------------------

On a slightly more detailed level service notifications contains two main call chains that can be hooked into for services to
perform their duties. The color of the arrows in the image below shows how data and
function calls flow through the peripheral context (red arrows)
and the application context (green arrows). The peripheral context and application context can
be seen as two seperate threads, or hardware interrupt context vs normal application context.

.. image:: notification_data_flow.png

The service data flow begins when a peripheral calls ``notify`` with
some peripheral-specific data. The ``basic_service`` (name may very well change)
will call ``notify_hook`` which will propogate up to the final ``application service``, forwarding
the data from the peripheral. The application service can use the data in whatever manner
it sees fit, for instance store it for forwarding to the completion handler etc.

Once the ``notify_hook`` call chain has finished the ``basic_service`` calls ``invoke_hook``, which takes no
arguments and should only be used to call the actual listener with its specific arguments.
This call chain can also be used to customize how and when the listener is called.
For instance ``executor_invoking_service`` will not directly call the next ``invoke_hook`` in the chain; it will
dispatch the call to the next ``invoke_hook`` to an ``execution_resource``. The ``execution_resource`` will invoke
``invoke_hook`` from within the ``run`` function, most likely on a completely different thread/context than
the one the peripheral called ``notify`` from.

This allows us to take data from a peripheral context and "move" it to an application context.
