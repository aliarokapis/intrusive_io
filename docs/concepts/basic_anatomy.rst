Basic Anatomy
=============

.. toctree::
  :hidden:

Intrusive IO is primarily concerned with asynchronous operations.
It does not rely on any operating system at all and is meant to be
usable on bare-metal devices.

Lets consider how a timeout timer by default works within this library.

.. image:: high_level_timeout_timer_concept.png

Your application must at least contain and run an execution resource.
It must also contain a service, a service user (timeout_timer) and a completion handler.

When your application uses the ``timeout_timer`` the flow of data/execution is as follows

#. Your application starts an async wait on the timeout timer.
#. ``timeout_timer`` starts an async listen on the timer service.
#. A timer peripheral notifies the ``timer_service``
#. When ``timer_service`` gets a notification, it dispatches it to all the listeners through their respective ``execution_resource``.
#. The ``execution_resource`` notifies the ``timeout_timer``.
#. If the ``timeout_timer`` has timed out it calls the completion handler, otherwise it goes back to step 2.
#. Your application **must** call ``execution_resource::run`` for service notifications to be processed correctly.

The asynchronous operations are driven by peripherals, where a peripheral can be some other thread, some hardware
peripheral that generates interrupts or something else that periodically notifes services.
Services acts as bridges between the peripherals and your application, using at least one ``execution_resource`` to
process service notifications and completion handlers.

