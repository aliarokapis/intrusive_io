.. Intrusive IO documentation master file, created by
   sphinx-quickstart on Mon Jun 10 16:00:30 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Intrusive IO's documentation!
========================================

.. note::
  This library is still very much a work-in-progress.

Contents:

.. toctree::
   :maxdepth: 2
   :includehidden:

   introduction
   concepts
   platform
   reference


Indices and tables
==================

* :ref:`genindex`
