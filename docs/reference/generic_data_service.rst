Generic data service
====================

.. toctree::
    :hidden:

The generic data service provides a generic
wrapper where all notification data is
forwarded to all listeners, going through
the executor associated with each listener.

Data is stored in a ``std::tuple`` and forwarded from
that.

Listener
--------

.. doxygenstruct:: intrusive_io::generic_data_service_listener
    :members:

Service
-------

.. doxygenclass:: intrusive_io::generic_data_service
    :members: