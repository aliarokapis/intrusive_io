Multilistener notifier
======================

.. toctree::
    :hidden:

An adapter to allow for one-to-many notification relationships.

.. doxygenclass:: intrusive_io::detail::multilistener_notifier_impl< ListenerT, basic_service_listener< notify_arguments< NotifyArgT... >, listener_arguments< ListenerArgT... > > >
    :members:
    :protected-members:
