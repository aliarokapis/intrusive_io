Basic service
=============

.. toctree::
    :hidden:

The base service that all other services and listeners derives from.

Listener
--------

.. doxygenclass:: intrusive_io::basic_service_listener< notify_arguments< NotifyArgs... >, listener_arguments< ListenerArgs... > >
    :members:
    :protected-members:

Service
-------

.. doxygenclass:: intrusive_io::detail::basic_service< ListenerT, notify_arguments< NotifyArgs... >, Locker >
    :members:
    :protected-members: