Introduction
============

.. toctree::
  :hidden:

Intrusive IO is an executor library targetting constrained devices, primarily
where dynamic memory management isn't available or viable.
The API aims to be close to
`boost::asio <https://www.boost.org/doc/libs/release/libs/asio/>`_
for developer familiarity. However the intrusiveness of the library puts some
constraints on it.


Main features include

* No allocations *at all*.
* No RTTI.
* No exceptions.
* Hooks to integrate with interrupt service routines.

Installation
------------

Clone the library with ``git clone git@gitlab.com:AndWass/intrusive_io.git``,
then run ``git submodule update --init --recursive`` to get all submodule
dependencies. Add `include` and `external/delegate/include` to your
include paths to start using the library.

The latest successful complete master release can be obtained from `here <https://andwass.gitlab.io/intrusive_io/release/master.zip>`_.

Dependencies
------------

The library depends on ``boost::intrusive`` and `span <https://github.com/martinmoene/span-lite>`_. 
All dependencies are header-only libraries.

.. note::
  Tests depends on `Catch2 <https://github.com/catchorg/Catch2>`_ and some samples depends on ``boost::asio``.
  These can be installed using ``conan install -o build_samples=True -o build_tests=True .``


Sample repeating timer
----------------------

The following sample is available in ``samples/timer/simple_repeating``

.. highlight:: c++

.. code-block:: c++

    #include "intrusive_io/execution_resource.hpp"
    #include "intrusive_io/timeout_timer.hpp"
    #include "intrusive_io/timer_service.hpp"
    
    #include <iostream>
    #include <thread>
    
    int main() {
        namespace io = intrusive_io;
        using namespace std::chrono_literals;
    
        // The executor will run all completion
        // handlers
        io::execution_resource executor;
    
        // The timer_service provides a simple
        // timer that timeout timers can use.
        io::timer_service<> timer_service;
    
        // Create a timeout timer and bind an executor and a service
        // to it. This object must not go out of scope while any
        // async operations are pending.
        io::timeout_timer timeout(executor, timer_service);
    
        // A service has no functionality of its own. We need
        // to notify the service whenever there is data available.
        // On a bare-metal device the service would typically be
        // triggered by a hardware interrupt. We use a std::thread
        // to simulate this.
        std::thread th([&]() {
            std::cout << "Notifying timer on thread " << std::this_thread::get_id() << std::endl;
            while (true) {
                std::this_thread::sleep_for(1s);
                timer_service.notify(intrusive_io::status_value::success, 1s);
            }
        });
    
        // Wait for 2 seconds before the lambda is invoked.
        timeout.async_wait(2s, [&timeout](intrusive_io::status_code error) {
            if (error.failure()) {
                std::cout << "Error!" << std::endl;
            }
            std::cout << "Timer timeout executed on thread " << std::this_thread::get_id() << std::endl;
            // Wait another 2 seconds, reusing the previously set handler.
            timeout.async_wait(2s);
        });
        executor.run();
    }


Those familiar with ``boost::asio`` can already see the similarities. This is an similar example
taken from the `boost::asio documentation <https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/tutorial/tuttimer2/src.html>`_.

.. code-block:: c++

  #include <iostream>
  #include <boost/asio.hpp>
  #include <boost/date_time/posix_time/posix_time.hpp>
  
  void print(const boost::system::error_code& /*e*/)
  {
    std::cout << "Hello, world!" << std::endl;
  }
  
  int main()
  {
    boost::asio::io_service io;
  
    boost::asio::deadline_timer t(io, boost::posix_time::seconds(5));
    t.async_wait(&print);
  
    io.run();
  
    return 0;
  }
