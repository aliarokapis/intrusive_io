===================
Platform adaptation
===================

.. toctree::
  :hidden:

The library is supposed to work using a variety of target platforms, either with or
without an (RT)OS. The library makes use of some platform-dependant types that cannot
be standardized accross all platforms, so each platform must provide types with the correct interfaces.

The ``intrusive_io/platform.hpp`` include file
must be modified to include the correct platform-dependant header.
The header must provide types with the following public interfaces.

.. highlight:: c++

.. code-block:: c++

   namespace intrusive_io::platform
   {
   class irq_safe_lock {
   public:
       void lock();
       void unlock();
   };
   class semaphore {
   public:
       void signal(); // Signal that work is available
       // Signal all that are potentially waiting on an extra predicate
       // this will not signal that work is available!
       void signal_all_waiting_on_predicate();

       // Perform a blocking wait, and will always return 1
       // indicating that work is available
       void wait();
       
       // Wait until either work is available, or predicate returns true.
       // This function will return true if work is available and false
       // if work isn't available.
       template <class PredicateT>
       bool wait(PredicateT &&predicate);

       bool try_wait(); // Perform a non-blocking "wait"
   };
   }

additionally, an ``irq_lock_guard`` RAII type, with the same functionality as
`std::lock_guard <https://en.cppreference.com/w/cpp/thread/lock_guard>`_ as well
as an ``atomic_bool`` must be provided.

Desktop platform
----------------

The desktop platform simply provides ``irq_safe_lock``,
``irq_lock_guard`` and ``atomic_bool`` as type-aliases:

.. code-block:: c++

  namespace intrusive_io::platform
  {
  using irq_safe_lock = std::mutex;
  using irq_lock_guard = std::lock_guard<std::mutex>;
  using atomic_bool = std::atomic<bool>;
  }

and a ``semaphore`` implementation based on
a ``std::condition_variable``.

Freestanding
------------

Look at ``include/intrusive_io/platform/freestanding/cortex-m34.hpp``
for an example of a freestanding OS-less implementation of the same
primitives.
