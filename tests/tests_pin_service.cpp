/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <catch2/catch.hpp>

#include "intrusive_io/execution_resource.hpp"
#include "intrusive_io/pin_service.hpp"

TEST_CASE("Pin service basic functionality", "[pin_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::pin_service ps;
    std::uint32_t pin = 0;
    bool level = false;
    intrusive_io::status_code status;
    auto handler = [&](intrusive_io::status_code code, std::uint32_t p, bool l) {
        status = code;
        pin = p;
        level = l;
    };
    auto listener =
        ps.make_listener(er, intrusive_io::wrap_functor(handler));
    ps.async_wait_pin_event(listener);
    ps.notify(23, true);
    REQUIRE(er.try_run() > 0);
    REQUIRE(status.success());
    REQUIRE(pin == 23);
    REQUIRE(level == true);
}

TEST_CASE("pin_service: cancelling sends operation_canceled code", "[pin_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::pin_service ps;
    std::uint32_t pin = 0;
    bool level = false;
    intrusive_io::status_code status;
    auto handler = [&](intrusive_io::status_code code, std::uint32_t p, bool l) {
        status = code;
        pin = p;
        level = l;
    };
    auto listener =
        ps.make_listener(er, intrusive_io::wrap_functor(handler));
    ps.async_wait_pin_event(listener);
    ps.cancel(listener);
    ps.notify(23, true);
    REQUIRE(er.try_run() == 1);
    REQUIRE(status.failure());
    REQUIRE(status.value() == intrusive_io::status_value::operation_canceled);
}
