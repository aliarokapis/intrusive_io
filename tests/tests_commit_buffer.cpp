#include <catch2/catch.hpp>
#include <numeric>

#include "intrusive_io/commit_span.hpp"

TEST_CASE("commit_buffer: default non-zero size commit buffer is empty", "[commit_buffer]")
{
    intrusive_io::commit_buffer<int, 1> test;
    REQUIRE(test.empty());
    REQUIRE(test.capacity() == 1);
    REQUIRE(test.size() == 0);
}

TEST_CASE("commit_buffer: default zero size commit buffer is not empty", "[commit_buffer]")
{
    intrusive_io::commit_buffer<int, 0> test;
    REQUIRE(!test.empty());
    REQUIRE(test.capacity() == 0);
    REQUIRE(test.size() == 0);
}

TEST_CASE("commit_buffer: (un)commited size is correct", "[commit_buffer]")
{
    intrusive_io::commit_buffer<int, 10> test;
    REQUIRE(test.uncommited_size() == 10);
    REQUIRE(test.uncommited_data().size() == 10);
    REQUIRE(test.capacity() == 10);

    REQUIRE(test.size() == 0);
    REQUIRE(test.commited_data().size() == 0);
}

TEST_CASE("commit_buffer: commiting data works", "[commit_buffer]")
{
    intrusive_io::commit_buffer<int, 10> test;
    test.commit(2);
    REQUIRE(test.commited_data().size() == 2);
    REQUIRE(test.size() == 2);
    REQUIRE(test.uncommited_size() == 8);
    REQUIRE(test.uncommited_data().size() == 8);

    REQUIRE(test.commited_data().data() == test.data());
    REQUIRE(std::data(test) == test.data());
    REQUIRE(test.uncommited_data().data() == test.data() + 2);

    REQUIRE(&(*test.begin()) == test.data());
    REQUIRE(&(*test.end()) == test.data()+2);
}

TEST_CASE("commit_buffer: full buffer returns empty commit area", "[commit_buffer]")
{
    intrusive_io::commit_buffer<int, 10> test;
    test.commit(10);
    REQUIRE(test.commited_data().size() == 10);
    REQUIRE(test.size() == 10);
    REQUIRE(test.uncommited_size() == 0);
    REQUIRE(test.uncommited_data().size() == 0);
    REQUIRE(test.full());
    REQUIRE(!test.empty());
    REQUIRE(std::size(test) == 10);
}

TEST_CASE("commit_buffer: iteration only iterates over commited area", "[commit_buffer]")
{
    intrusive_io::commit_buffer<int, 10> test;
    auto uncommited = test.uncommited_data();
    std::size_t i=0;
    uncommited[i++] = 1;
    uncommited[i++] = 2;
    uncommited[i++] = 3;
    uncommited[i++] = 4;
    uncommited[i++] = 5;
    uncommited[i++] = 6;
    uncommited[i++] = 7;

    test.commit(1);
    auto acc = std::accumulate(test.begin(), test.end(), 0);
    REQUIRE(acc == 1);
    
    test.commit(2);
    acc = std::accumulate(test.begin(), test.end(), 0);
    REQUIRE(acc == 6);
    acc = std::accumulate(test.rbegin(), test.rend(), 0);
    REQUIRE(acc == 6);

    REQUIRE(uncommited[0] == 1);
    REQUIRE(uncommited[1] == 2);
    REQUIRE(uncommited[2] == 3);
}

TEST_CASE("commit_buffer: erase tests", "[commit_buffer]")
{
    intrusive_io::commit_buffer<int, 10> test;
    auto uncommited = test.uncommited_data();
    std::size_t i=0;
    uncommited[i++] = 1;
    uncommited[i++] = 2;
    uncommited[i++] = 3;
    uncommited[i++] = 4;
    uncommited[i++] = 5;
    uncommited[i++] = 6;
    uncommited[i++] = 7;
    test.commit(i);
    REQUIRE(test.size() == 7);

    SECTION("erase at beginning")
    {
        test.erase(test.begin(), test.begin() + 3);
        REQUIRE(test.size() == 4);
        REQUIRE(test[0] == 4);
    }
    SECTION("erase at end")
    {
        test.erase(test.begin() + 4, test.end());
        REQUIRE(test.size() == 4);
        REQUIRE(test[0] == 1);
        REQUIRE(test[3] == 4);
    }

    SECTION("erase in middle")
    {
        test.erase(test.begin() + 2, test.begin() + 5);
        REQUIRE(test.size() == 4);
        REQUIRE(test[0] == 1);
        REQUIRE(test[1] == 2);
        REQUIRE(test[2] == 6);
        REQUIRE(test[3] == 7);
    }
}