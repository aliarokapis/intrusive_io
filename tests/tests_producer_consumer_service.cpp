#include <array>
#include <catch2/catch.hpp>

#include "intrusive_io/producer_consumer_service.hpp"
#include "intrusive_io/span.hpp"

TEST_CASE("Single produced data is consumed", "[producer_consumer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::producer_consumer_service<intrusive_io::span<std::uint8_t>> service;

    std::array<std::uint8_t, 5> data;
    data.fill(6);
    std::array<std::uint8_t, 5> consumed_data;
    intrusive_io::status_code consumer_status;

    std::size_t data_consumed = 0;
    auto consumer_completion = [&data_consumed, &consumed_data,
                                &consumer_status](intrusive_io::status_code code,
                                                  intrusive_io::span<std::uint8_t> data) {
        data_consumed = data.size();
        REQUIRE(data.size() == consumed_data.size());
        std::copy(data.begin(), data.end(), consumed_data.begin());
        consumer_status = code;
    };

    bool producer_done = false;
    intrusive_io::status_code status;
    auto producer_completion =
        [&producer_done, &status](intrusive_io::status_code code,
                                  intrusive_io::data_producer<intrusive_io::span<std::uint8_t>> *) {
            producer_done = true;
            status = code;
        };

    auto producer = service.make_producer(er, intrusive_io::wrap_functor(producer_completion));

    auto consumer = service.make_consumer(er, intrusive_io::wrap_functor(consumer_completion));

    service.async_consume_data(consumer);
    REQUIRE(data_consumed == 0);
    REQUIRE(er.try_run() == 0);

    service.async_produce_data(producer, data);
    REQUIRE(data_consumed == 0);
    REQUIRE(producer_done == false);
    REQUIRE(er.try_run() != 0);
    REQUIRE(data_consumed == 5);
    REQUIRE(data == consumed_data);

    service.notify_consumed_done(intrusive_io::status_value::success, consumer);
    REQUIRE_FALSE(producer_done);
    REQUIRE(er.try_run() != 0);
    REQUIRE(producer_done);
    REQUIRE(status.success());
}

TEST_CASE("Multiple produced data is one after the other", "[producer_consumer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::producer_consumer_service<int> service;

    int consumed_data = 0;

    auto consumer_completion = [&consumed_data](intrusive_io::status_code code, int data) {
        consumed_data = data;
    };

    bool producer_done1 = false;
    auto producer_completion1 = [&producer_done1](intrusive_io::status_code code,
                                                  intrusive_io::data_producer<int> *) {
        producer_done1 = true;
    };

    bool producer_done2 = false;
    auto producer_completion2 = [&producer_done2](intrusive_io::status_code code,
                                                  intrusive_io::data_producer<int> *) {
        producer_done2 = true;
    };

    auto producer1 = service.make_producer(er, intrusive_io::wrap_functor(producer_completion1));

    auto producer2 = service.make_producer(er, intrusive_io::wrap_functor(producer_completion2));

    auto consumer = service.make_consumer(er, intrusive_io::wrap_functor(consumer_completion));

    service.async_consume_data(consumer);
    REQUIRE(consumed_data == 0);
    REQUIRE(er.try_run() == 0);

    service.async_produce_data(producer1, 1);
    service.async_produce_data(producer2, 2);
    REQUIRE(consumed_data == 0);
    REQUIRE(producer_done1 == false);
    REQUIRE(producer1.is_in_use());
    REQUIRE(producer2.is_in_use());

    REQUIRE(er.try_run() != 0);
    REQUIRE(consumed_data == 1);
    REQUIRE_FALSE(producer_done1);
    REQUIRE(producer1.is_in_use());
    REQUIRE(producer2.is_in_use());
    service.notify_consumed_done(intrusive_io::status_value::success, consumer);
    REQUIRE(er.try_run() != 0);
    REQUIRE(producer_done1);
    REQUIRE_FALSE(producer1.is_in_use());
    REQUIRE(producer2.is_in_use());
    REQUIRE_FALSE(producer_done2);

    service.async_consume_data(consumer);
    REQUIRE(er.try_run() != 0);
    REQUIRE(consumed_data == 2);
    REQUIRE(producer_done1);
    REQUIRE_FALSE(producer_done2);
    REQUIRE_FALSE(producer1.is_in_use());
    REQUIRE(producer2.is_in_use());
    service.notify_consumed_done(intrusive_io::status_value::success, consumer);
    REQUIRE(er.try_run() != 0);
    REQUIRE(producer_done2);
    REQUIRE_FALSE(producer1.is_in_use());
    REQUIRE_FALSE(producer2.is_in_use());
}

TEST_CASE("Producer consumer listeners default constructible", "[producer_consumer_service]") {
    intrusive_io::data_producer<int> producer;
    intrusive_io::data_consumer<int> consumer;
}

TEST_CASE("producer_consumer_service: cancelling waiting consumer works",
          "[producer_consumer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::producer_consumer_service<int> service;

    int data = 0;
    intrusive_io::status_code consumer_status(intrusive_io::status_value::unknown);
    auto consumer_completion = [&consumer_status](intrusive_io::status_code code, int data) {
        consumer_status = code;
    };

    bool producer_done = false;
    auto producer_completion = [&producer_done](intrusive_io::status_code code,
                                                intrusive_io::data_producer<int> *) {
        producer_done = true;
    };

    auto consumer = service.make_consumer(er, intrusive_io::wrap_functor(consumer_completion));
    service.async_consume_data(consumer);
    REQUIRE(er.try_run() == 0);
    service.cancel(consumer);
    REQUIRE(er.try_run() > 0);
    REQUIRE(consumer_status.value() == intrusive_io::status_value::operation_canceled);

    auto producer = service.make_producer(er, intrusive_io::wrap_functor(producer_completion));
    service.async_produce_data(producer, 5);
    er.try_run();
    REQUIRE_FALSE(producer_done);
    REQUIRE(consumer_status.value() == intrusive_io::status_value::operation_canceled);

    service.async_consume_data(consumer);
    er.try_run();
    service.cancel(consumer);
    REQUIRE(er.try_run() == 0);
    REQUIRE(consumer_status.value() == intrusive_io::status_value::success);
    service.notify_consumed_done(intrusive_io::status_value::success, consumer);
    REQUIRE(er.try_run() > 0);
    REQUIRE(producer_done);
}

TEST_CASE("producer_consumer_service: cancelling waiting producer works",
          "[producer_consumer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::producer_consumer_service<int> service;

    int data = 0;
    intrusive_io::status_code producer_status(intrusive_io::status_value::unknown);
    bool consumer_done = false;
    auto consumer_completion = [&](intrusive_io::status_code code, int d) {
        consumer_done = true;
        data = d;
    };

    bool producer_done = false;
    auto producer_completion = [&producer_done,
                                &producer_status](intrusive_io::status_code code,
                                                  intrusive_io::data_producer<int> *) {
        producer_done = true;
        producer_status = code;
    };

    auto producer = service.make_producer(er, intrusive_io::wrap_functor(producer_completion));
    service.async_produce_data(producer, 5);
    REQUIRE(er.try_run() == 0);
    service.cancel(producer);
    REQUIRE(er.try_run() > 0);
    REQUIRE(producer_done);
    REQUIRE(producer_status.value() == intrusive_io::status_value::operation_canceled);

    auto consumer = service.make_consumer(er, intrusive_io::wrap_functor(consumer_completion));
    service.async_consume_data(consumer);
    er.try_run();
    REQUIRE_FALSE(consumer_done);

    producer_done = false;
    service.async_produce_data(producer, 10);
    er.try_run();
    REQUIRE(consumer_done);
    service.notify_consumed_done(intrusive_io::status_value::success, consumer);
    er.try_run();
    REQUIRE(producer_done);
    REQUIRE(producer_status.value() == intrusive_io::status_value::success);
}

TEST_CASE("producer_consumer_service: direct consume behaves as expected.",
          "[producer_consumer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::producer_consumer_service<int> service;

    int consumed_data = 0;
    auto producer = service.make_producer(er, {});
    auto consumer_handler = [&](intrusive_io::status_code sc, int data) {
        consumed_data = data;
    };
    auto consumer = service.make_consumer(er, intrusive_io::wrap_functor(consumer_handler));

    service.async_produce_data(producer, 5);
    service.async_consume_data(consumer, intrusive_io::dispatch_policy::direct);
    REQUIRE(consumed_data == 5);
    REQUIRE(producer.is_in_use());
    service.notify_consumed_done(intrusive_io::status_value::success, consumer);
    er.try_run();
    REQUIRE(!producer.is_in_use());
}