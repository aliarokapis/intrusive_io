#include "catch2/catch.hpp"

#include "intrusive_io/data_buffer_service.hpp"

#include <array>
#include <string>

TEST_CASE("Will correctly notify a single byte read", "[data_buffer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::data_buffer_service<std::uint8_t, 16> service(er);
    intrusive_io::status_code status;

    std::size_t bytes_read = 0;
    auto handler = [&](intrusive_io::status_code code, intrusive_io::span<std::uint8_t> data) {
        bytes_read = data.size();
        status = code;
    };

    std::array<std::uint8_t, 10> data;
    data.fill(0);

    auto listener = service.make_listener(er, intrusive_io::wrap_functor(handler));
    service.async_read_some(listener, data);
    service.notify(10);

    er.try_run();
    REQUIRE(status.success());
    REQUIRE(bytes_read == 1);
    REQUIRE(data[0] == 10);

    service.async_read_some(listener, data);
    service.notify(20);
    er.try_run();

    REQUIRE(status.success());
    REQUIRE(bytes_read == 1);
    REQUIRE(data[0] == 20);
}

TEST_CASE("Notify shall dispatch to executor", "[data_buffer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::data_buffer_service<std::uint8_t, 16> service(er);

    service.notify(10);
    REQUIRE(er.try_run() == 1);
    service.notify(20);
    REQUIRE(er.try_run() == 1);
    service.notify(30);
    REQUIRE(er.try_run() == 1);
    service.notify(40);
    REQUIRE(er.try_run() == 1);
}

TEST_CASE("Will buffer data when no listeners are available", "[data_buffer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::data_buffer_service<std::uint8_t, 16> service(er);

    std::size_t bytes_read = 0;
    intrusive_io::status_code status;
    auto handler = [&](intrusive_io::status_code code, intrusive_io::span<std::uint8_t> data) {
        status = code;
        bytes_read = data.size();
    };

    std::array<std::uint8_t, 10> data;
    data.fill(0);

    service.notify(10);

    auto listener = service.make_listener(er, intrusive_io::wrap_functor(handler));
    service.async_read_some(listener, data);

    er.try_run();
    REQUIRE(status.success());
    REQUIRE(bytes_read == 1);
    REQUIRE(data[0] == 10);

    service.notify(20);
    service.notify(30);
    service.notify(40);
    service.notify(50);

    service.async_read_some(listener, data);
    er.try_run();
    REQUIRE(status.success());
    REQUIRE(bytes_read == 4);
    REQUIRE(data[0] == 20);
    REQUIRE(data[1] == 30);
    REQUIRE(data[2] == 40);
    REQUIRE(data[3] == 50);
}

TEST_CASE("Will not empty buffer when no listeners are available", "[data_buffer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::data_buffer_service<std::uint8_t, 16> service(er);

    std::size_t bytes_read = 0;
    intrusive_io::status_code status;
    auto handler = [&](intrusive_io::status_code code, intrusive_io::span<std::uint8_t> data) {
        status = code;
        bytes_read = data.size();
    };

    std::array<std::uint8_t, 10> data;
    data.fill(0);

    auto listener = service.make_listener(er, intrusive_io::wrap_functor(handler));

    service.notify(20);
    er.try_run();
    service.notify(30);
    er.try_run();
    service.notify(40);
    er.try_run();
    service.notify(50);
    er.try_run();

    service.async_read_some(listener, data);
    REQUIRE(bytes_read == 0);
    er.try_run();
    REQUIRE(status.success());
    REQUIRE(bytes_read == 4);
    REQUIRE(data[0] == 20);
    REQUIRE(data[1] == 30);
    REQUIRE(data[2] == 40);
    REQUIRE(data[3] == 50);
}

TEST_CASE("Buffering of std::string works", "[data_buffer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::data_buffer_service<std::string, 16> service(er);

    std::size_t items_read = 0;
    intrusive_io::status_code status;
    auto handler = [&](intrusive_io::status_code code, intrusive_io::span<std::string> data) {
        status = code;
        items_read = data.size();
    };

    std::array<std::string, 10> data;

    auto listener = service.make_listener(er, intrusive_io::wrap_functor(handler));

    service.notify("hello");
    er.try_run();
    service.notify("world");
    er.try_run();
    service.notify("123");
    er.try_run();
    service.notify("456");
    er.try_run();

    service.async_read_some(listener, data);
    REQUIRE(items_read == 0);
    er.try_run();
    REQUIRE(status.success());
    REQUIRE(items_read == 4);
    REQUIRE(data[0] == "hello");
    REQUIRE(data[1] == "world");
    REQUIRE(data[2] == "123");
    REQUIRE(data[3] == "456");
}

TEST_CASE("data_buffer_service: cancelling does not consume data", "[data_buffer_service]") {
    intrusive_io::execution_resource er;
    intrusive_io::data_buffer_service<std::string, 16> service(er);

    std::size_t items_read = 0;
    intrusive_io::status_code status(intrusive_io::status_value::unknown);
    auto handler = [&](intrusive_io::status_code code, intrusive_io::span<std::string> data) {
        status = code;
        items_read = data.size();
    };

    std::array<std::string, 10> data;

    auto listener = service.make_listener(er, intrusive_io::wrap_functor(handler));

    service.async_read_some(listener, data);
    REQUIRE(items_read == 0);
    REQUIRE(er.try_run() == 0);
    service.cancel(listener);
    REQUIRE(er.try_run() > 0);
    REQUIRE(status.failure());
    REQUIRE(status.value() == intrusive_io::status_value::operation_canceled);
}

TEST_CASE("Data buffer service listeners default constructible", "[data_buffer_service]") {
    intrusive_io::data_buffer_service_listener<std::uint8_t> listener;
}
