from conans import ConanFile, CMake

class IntrusiveIoConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "boost/[>=1.70]@conan/stable"
    generators = "cmake"
    options = {"build_samples": [True, False]}
    default_options = {"boost:header_only": True, "build_samples": False}
    def configure(self):
        if self.options.build_samples:
            self.options["boost"].header_only = False

    def requirements(self):
        return
